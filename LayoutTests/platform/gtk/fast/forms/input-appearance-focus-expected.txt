layer at (0,0) size 800x600
  RenderView at (0,0) size 800x600
layer at (0,0) size 800x600
  RenderBlock {HTML} at (0,0) size 800x600
    RenderBody {BODY} at (8,8) size 784x576
      RenderBlock (anonymous) at (0,0) size 784x19
        RenderText {#text} at (0,0) size 755x19
          text run at (0,0) width 755: "This test uses the new text field to test focus() and blur() and to make sure that onFocus and onBlur events fire correctly."
      RenderBlock {P} at (0,35) size 784x29
        RenderTextControl {INPUT} at (2,2) size 194x25 [bgcolor=#FFFFFF] [border: (2px inset #000000)]
        RenderText {#text} at (198,5) size 4x19
          text run at (198,5) width 4: " "
        RenderTextControl {INPUT} at (204,2) size 194x25 [bgcolor=#FFFFFF] [border: (2px inset #000000)]
        RenderText {#text} at (0,0) size 0x0
      RenderBlock {DIV} at (0,80) size 784x76
        RenderBR {BR} at (0,0) size 0x19
        RenderText {#text} at (0,19) size 322x19
          text run at (0,19) width 322: "Test Passed. Text field 1's onFocus event has fired."
        RenderBR {BR} at (322,34) size 0x0
        RenderText {#text} at (0,38) size 312x19
          text run at (0,38) width 312: "Test Passed. Text field 1's onBlur event has fired."
        RenderBR {BR} at (312,53) size 0x0
        RenderText {#text} at (0,57) size 322x19
          text run at (0,57) width 322: "Test Passed. Text field 2's onFocus event has fired."
      RenderBlock {P} at (0,172) size 784x0
layer at (13,48) size 188x19
  RenderBlock {DIV} at (3,3) size 188x19
    RenderText {#text} at (1,0) size 109x19
      text run at (1,0) width 109: "My Text Field 1"
layer at (215,48) size 188x19
  RenderBlock {DIV} at (3,3) size 188x19
    RenderText {#text} at (1,0) size 109x19
      text run at (1,0) width 109: "My Text Field 2"
selection start: position 0 of child 0 {#text} of child 0 {DIV} of child 3 {INPUT} of child 1 {P} of body
selection end:   position 15 of child 0 {#text} of child 0 {DIV} of child 3 {INPUT} of child 1 {P} of body
