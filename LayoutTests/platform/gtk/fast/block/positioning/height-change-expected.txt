layer at (0,0) size 800x600
  RenderView at (0,0) size 800x600
layer at (0,0) size 800x600
  RenderBlock {HTML} at (0,0) size 800x600
    RenderBody {BODY} at (8,8) size 784x584
      RenderBlock {P} at (0,0) size 784x38
        RenderText {#text} at (0,0) size 212x19
          text run at (0,0) width 212: "This tests for a regression against "
        RenderInline {I} at (0,0) size 743x38
          RenderInline {A} at (0,0) size 348x19 [color=#0000EE]
            RenderText {#text} at (212,0) size 348x19
              text run at (212,0) width 348: "http://bugzilla.opendarwin.org/show_bug.cgi?id=6881"
          RenderText {#text} at (560,0) size 743x38
            text run at (560,0) width 4: " "
            text run at (564,0) width 179: "Block with position:absolute"
            text run at (0,19) width 579: "bottom:0 doesn't always move when height of containing block changes (affects Safari RSS)"
        RenderText {#text} at (579,19) size 4x19
          text run at (579,19) width 4: "."
      RenderBlock {P} at (0,54) size 784x19
        RenderText {#text} at (0,0) size 550x19
          text run at (0,0) width 550: "The squares below should have blue tops and green bottoms, not the other way around."
      RenderBlock {HR} at (0,89) size 784x2 [border: (1px inset #000000)]
      RenderBlock (anonymous) at (0,199) size 784x19
        RenderBR {BR} at (0,0) size 0x19
layer at (8,107) size 784x100
  RenderBlock (relative positioned) {DIV} at (0,99) size 784x100
    RenderBlock {DIV} at (0,0) size 100x100 [bgcolor=#87CEEB]
layer at (8,157) size 100x50
  RenderBlock (positioned) {DIV} at (0,50) size 100x50 [bgcolor=#90EE90]
layer at (8,226) size 784x100
  RenderFlexibleBox (relative positioned) {DIV} at (0,218) size 784x100
    RenderBlock {DIV} at (0,0) size 100x100 [bgcolor=#87CEEB]
layer at (8,276) size 100x50
  RenderBlock (positioned) {DIV} at (0,50) size 100x50 [bgcolor=#90EE90]
