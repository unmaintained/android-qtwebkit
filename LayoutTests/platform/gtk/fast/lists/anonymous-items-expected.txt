layer at (0,0) size 800x600
  RenderView at (0,0) size 800x600
layer at (0,0) size 800x600
  RenderBlock {HTML} at (0,0) size 800x600
    RenderBody {BODY} at (8,8) size 784x576
      RenderBlock {P} at (0,0) size 784x19
        RenderText {#text} at (0,0) size 395x19
          text run at (0,0) width 395: "Tests list item numbering when there are anonymous list items."
      RenderBlock {OL} at (0,35) size 784x19
        RenderListItem at (40,0) size 744x19
          RenderListMarker at (-21,0) size 16x19: "1"
          RenderText at (0,0) size 34x19
            text run at (0,0) width 34: "ONE"
      RenderBlock {OL} at (0,70) size 784x38
        RenderListItem {LI} at (40,0) size 744x19
          RenderListMarker at (-21,0) size 16x19: "1"
          RenderText {#text} at (0,0) size 23x19
            text run at (0,0) width 23: "one"
        RenderListItem at (40,19) size 744x19
          RenderListMarker at (-21,0) size 16x19: "2"
          RenderText at (0,0) size 37x19
            text run at (0,0) width 37: "TWO"
      RenderBlock {OL} at (0,124) size 784x19
        RenderListItem at (40,0) size 744x19
          RenderListMarker at (-21,0) size 16x19: "1"
          RenderText at (0,0) size 34x19
            text run at (0,0) width 34: "ONE"
      RenderBlock {OL} at (0,159) size 784x38
        RenderListItem at (40,0) size 744x19
          RenderListMarker at (-21,0) size 16x19: "1"
          RenderText at (0,0) size 34x19
            text run at (0,0) width 34: "ONE"
        RenderListItem {LI} at (40,19) size 744x19
          RenderListMarker at (-21,0) size 16x19: "2"
          RenderText {#text} at (0,0) size 24x19
            text run at (0,0) width 24: "two"
      RenderBlock {OL} at (0,213) size 784x76
        RenderListItem {LI} at (40,0) size 744x19
          RenderListMarker at (-21,0) size 16x19: "1"
          RenderText {#text} at (0,0) size 23x19
            text run at (0,0) width 23: "one"
        RenderBlock {DIV} at (40,19) size 744x38
          RenderBlock (anonymous) at (0,0) size 744x19
            RenderText {#text} at (0,0) size 20x19
              text run at (0,0) width 20: "div"
          RenderListItem at (0,19) size 744x19
            RenderListMarker at (-21,0) size 16x19: "2"
            RenderText at (0,0) size 37x19
              text run at (0,0) width 37: "TWO"
        RenderListItem {LI} at (40,57) size 744x19
          RenderListMarker at (-21,0) size 16x19: "3"
          RenderText {#text} at (0,0) size 31x19
            text run at (0,0) width 31: "three"
      RenderBlock {OL} at (0,305) size 784x76
        RenderListItem {LI} at (40,0) size 744x19
          RenderListMarker at (-21,0) size 16x19: "1"
          RenderText {#text} at (0,0) size 23x19
            text run at (0,0) width 23: "one"
        RenderBlock {DIV} at (40,19) size 744x38
          RenderListItem at (0,0) size 744x19
            RenderListMarker at (-21,0) size 16x19: "2"
            RenderText at (0,0) size 37x19
              text run at (0,0) width 37: "TWO"
          RenderBlock (anonymous) at (0,19) size 744x19
            RenderText {#text} at (0,0) size 20x19
              text run at (0,0) width 20: "div"
        RenderListItem {LI} at (40,57) size 744x19
          RenderListMarker at (-21,0) size 16x19: "3"
          RenderText {#text} at (0,0) size 31x19
            text run at (0,0) width 31: "three"
