layer at (0,0) size 784x687
  RenderView at (0,0) size 784x600
layer at (0,0) size 784x687
  RenderBlock {HTML} at (0,0) size 784x687
    RenderBody {BODY} at (8,8) size 768x671
      RenderBlock {H1} at (0,0) size 768x42
        RenderText {#text} at (0,0) size 608x42
          text run at (0,0) width 608: "Fixed Columns, Auto Span, Minwidth Table"
      RenderTable {TABLE} at (0,63) size 100x40
        RenderTableSection {TBODY} at (0,0) size 100x40
          RenderTableRow {TR} at (0,0) size 100x20
            RenderTableCell {TD} at (0,10) size 33x0 [bgcolor=#00FFFF] [r=0 c=0 rs=1 cs=1]
            RenderTableCell {TD} at (33,10) size 67x0 [bgcolor=#FFFF00] [r=0 c=1 rs=1 cs=1]
          RenderTableRow {TR} at (0,20) size 100x20
            RenderTableCell {TD} at (0,30) size 100x0 [bgcolor=#FFC0CB] [r=1 c=0 rs=1 cs=2]
              RenderBlock {DIV} at (0,0) size 100x0
      RenderBlock {P} at (0,119) size 768x66
        RenderText {#text} at (0,0) size 154x22
          text run at (0,0) width 154: "The table width is: 100"
        RenderBR {BR} at (154,16) size 0x0
        RenderText {#text} at (0,22) size 142x22
          text run at (0,22) width 142: "Column One is: 33%"
        RenderBR {BR} at (142,38) size 0x0
        RenderText {#text} at (0,44) size 145x22
          text run at (0,44) width 145: "Column Two is: 67%"
      RenderBlock {HR} at (0,201) size 768x2 [border: (1px inset #000000)]
      RenderTable {TABLE} at (0,211) size 600x40
        RenderTableSection {TBODY} at (0,0) size 600x40
          RenderTableRow {TR} at (0,0) size 600x20
            RenderTableCell {TD} at (0,10) size 200x0 [bgcolor=#00FFFF] [r=0 c=0 rs=1 cs=1]
            RenderTableCell {TD} at (200,10) size 400x0 [bgcolor=#FFFF00] [r=0 c=1 rs=1 cs=1]
          RenderTableRow {TR} at (0,20) size 600x20
            RenderTableCell {TD} at (0,30) size 600x0 [bgcolor=#FFC0CB] [r=1 c=0 rs=1 cs=2]
              RenderBlock {DIV} at (0,0) size 600x0
      RenderBlock {P} at (0,267) size 768x66
        RenderText {#text} at (0,0) size 154x22
          text run at (0,0) width 154: "The table width is: 600"
        RenderBR {BR} at (154,16) size 0x0
        RenderText {#text} at (0,22) size 142x22
          text run at (0,22) width 142: "Column One is: 33%"
        RenderBR {BR} at (142,38) size 0x0
        RenderText {#text} at (0,44) size 145x22
          text run at (0,44) width 145: "Column Two is: 67%"
      RenderBlock {HR} at (0,349) size 768x2 [border: (1px inset #000000)]
      RenderTable {TABLE} at (0,359) size 600x64
        RenderTableSection {TBODY} at (0,0) size 600x64
          RenderTableRow {TR} at (0,0) size 600x44
            RenderTableCell {TD} at (0,0) size 200x44 [bgcolor=#00FFFF] [r=0 c=0 rs=1 cs=1]
              RenderText {#text} at (0,0) size 197x44
                text run at (0,0) width 197: "Fixed cell in column one with"
                text run at (0,22) width 67: "some text."
            RenderTableCell {TD} at (200,0) size 400x44 [bgcolor=#FFFF00] [r=0 c=1 rs=1 cs=1]
              RenderText {#text} at (0,0) size 360x44
                text run at (0,0) width 360: "Fixed cell in column two with a lot more text. Will the"
                text run at (0,22) width 137: "ratios be preserved?"
          RenderTableRow {TR} at (0,44) size 600x20
            RenderTableCell {TD} at (0,54) size 600x0 [bgcolor=#FFC0CB] [r=1 c=0 rs=1 cs=2]
              RenderBlock {DIV} at (0,0) size 600x0
      RenderBlock {P} at (0,439) size 768x66
        RenderText {#text} at (0,0) size 154x22
          text run at (0,0) width 154: "The table width is: 600"
        RenderBR {BR} at (154,16) size 0x0
        RenderText {#text} at (0,22) size 142x22
          text run at (0,22) width 142: "Column One is: 33%"
        RenderBR {BR} at (142,38) size 0x0
        RenderText {#text} at (0,44) size 145x22
          text run at (0,44) width 145: "Column Two is: 67%"
      RenderBlock {HR} at (0,521) size 768x2 [border: (1px inset #000000)]
      RenderTable {TABLE} at (0,531) size 600x40
        RenderTableSection {TBODY} at (0,0) size 600x40
          RenderTableRow {TR} at (0,0) size 600x20
            RenderTableCell {TD} at (0,10) size 200x0 [bgcolor=#00FFFF] [r=0 c=0 rs=1 cs=1]
              RenderBlock {DIV} at (0,0) size 100x0
            RenderTableCell {TD} at (200,10) size 400x0 [bgcolor=#FFFF00] [r=0 c=1 rs=1 cs=1]
              RenderBlock {DIV} at (0,0) size 250x0
          RenderTableRow {TR} at (0,20) size 600x20
            RenderTableCell {TD} at (0,30) size 600x0 [bgcolor=#FFC0CB] [r=1 c=0 rs=1 cs=2]
              RenderBlock {DIV} at (0,0) size 600x0
      RenderBlock {P} at (0,587) size 768x66
        RenderText {#text} at (0,0) size 154x22
          text run at (0,0) width 154: "The table width is: 600"
        RenderBR {BR} at (154,16) size 0x0
        RenderText {#text} at (0,22) size 142x22
          text run at (0,22) width 142: "Column One is: 33%"
        RenderBR {BR} at (142,38) size 0x0
        RenderText {#text} at (0,44) size 145x22
          text run at (0,44) width 145: "Column Two is: 67%"
      RenderBlock {HR} at (0,669) size 768x2 [border: (1px inset #000000)]
