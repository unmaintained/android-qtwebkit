layer at (0,0) size 800x600
  RenderView at (0,0) size 800x600
layer at (0,0) size 800x600
  RenderBlock {HTML} at (0,0) size 800x600
    RenderBody {BODY} at (8,8) size 784x582
      RenderBlock {P} at (0,0) size 784x22
        RenderInline {EM} at (0,0) size 646x22
          RenderText {#text} at (0,0) size 646x22
            text run at (0,0) width 646: "When an :after rule has multiple pieces of content, older versions of Safari display them backwards."
      RenderBlock {DIV} at (10,38) size 764x53 [border: (1px solid #000000)]
        RenderText {#text} at (11,20) size 48x22
          text run at (11,20) width 48: "Inline: "
        RenderInline (generated) at (0,0) size 514x22
          RenderText at (59,20) size 250x22
            text run at (59,20) width 250: "This should be before the green box. "
          RenderImage at (309,11) size 25x25
          RenderText at (334,20) size 239x22
            text run at (334,20) width 239: " This should be after the green box."
      RenderBlock {DIV} at (10,101) size 764x97 [border: (1px solid #000000)]
        RenderText {#text} at (11,64) size 90x22
          text run at (11,64) width 90: "Inline-block: "
        RenderBlock (generated) at (101,11) size 200x75
          RenderText at (0,0) size 171x53
            text run at (0,0) width 171: "This should be before the"
            text run at (0,31) width 75: "green box. "
          RenderImage at (75,22) size 25x25
          RenderText at (100,31) size 183x44
            text run at (100,31) width 83: " This should"
            text run at (0,53) width 152: "be after the green box."
      RenderBlock {DIV} at (10,208) size 764x119 [border: (1px solid #000000)]
        RenderBlock (anonymous) at (11,11) size 742x22
          RenderText {#text} at (0,0) size 44x22
            text run at (0,0) width 44: "Block:"
        RenderBlock (generated) at (11,33) size 200x75
          RenderText at (0,0) size 171x53
            text run at (0,0) width 171: "This should be before the"
            text run at (0,31) width 75: "green box. "
          RenderImage at (75,22) size 25x25
          RenderText at (100,31) size 183x44
            text run at (100,31) width 83: " This should"
            text run at (0,53) width 152: "be after the green box."
