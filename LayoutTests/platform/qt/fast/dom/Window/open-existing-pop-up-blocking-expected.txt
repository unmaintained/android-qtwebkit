layer at (0,0) size 800x600
  RenderView at (0,0) size 800x600
layer at (0,0) size 800x600
  RenderBlock {HTML} at (0,0) size 800x600
    RenderBody {BODY} at (8,8) size 784x584
      RenderPartObject {IFRAME} at (0,0) size 304x154 [border: (2px inset #000000)]
        layer at (0,0) size 300x150
          RenderView at (0,0) size 300x150
        layer at (0,0) size 300x150
          RenderBlock {HTML} at (0,0) size 300x150
            RenderBody {BODY} at (8,8) size 284x134
              RenderText {#text} at (0,0) size 281x88
                text run at (0,0) width 281: "This frame will try to replace the contents"
                text run at (0,22) width 262: "of the frame to the right. If the bug still"
                text run at (0,44) width 260: "occurs, pop-up blocking will prevent it"
                text run at (0,66) width 97: "from doing so."
      RenderText {#text} at (304,138) size 4x22
        text run at (304,138) width 4: " "
      RenderPartObject {IFRAME} at (308,0) size 304x154 [border: (2px inset #000000)]
        layer at (0,0) size 300x150
          RenderView at (0,0) size 300x150
        layer at (0,0) size 300x150
          RenderBlock {HTML} at (0,0) size 300x150
            RenderBody {BODY} at (8,8) size 284x134
              RenderText {#text} at (0,0) size 284x44
                text run at (0,0) width 284: "This text successfully replaced the original"
                text run at (0,22) width 225: "text in the frame. Test succeeded!"
      RenderText {#text} at (0,0) size 0x0
