layer at (0,0) size 800x600
  RenderView at (0,0) size 800x600
layer at (0,0) size 800x600
  RenderBlock {HTML} at (0,0) size 800x600
    RenderBody {BODY} at (8,8) size 784x584
      RenderTable {TABLE} at (0,0) size 666x60 [border: (1px outset #808080)]
        RenderTableCol {COLGROUP} at (0,0) size 0x0
          RenderTableCol {COL} at (0,0) size 0x0
          RenderTableCol {COL} at (0,0) size 0x0
          RenderTableCol {COL} at (0,0) size 0x0
        RenderTableSection {TBODY} at (1,1) size 664x58
          RenderTableRow {TR} at (0,2) size 664x26
            RenderTableCell {TD} at (2,2) size 500x26 [border: (1px inset #808080)] [r=0 c=0 rs=1 cs=1]
              RenderText {#text} at (2,2) size 74x22
                text run at (2,2) width 74: "col 1 row 1"
            RenderTableCell {TD} at (504,2) size 78x26 [border: (1px inset #808080)] [r=0 c=1 rs=1 cs=1]
              RenderText {#text} at (2,2) size 74x22
                text run at (2,2) width 74: "col 2 row 1"
            RenderTableCell {TD} at (584,2) size 78x26 [border: (1px inset #808080)] [r=0 c=2 rs=1 cs=1]
              RenderText {#text} at (2,2) size 74x22
                text run at (2,2) width 74: "col 3 row 1"
          RenderTableRow {TR} at (0,30) size 664x26
            RenderTableCell {TD} at (2,30) size 500x26 [border: (1px inset #808080)] [r=1 c=0 rs=1 cs=1]
              RenderText {#text} at (2,2) size 74x22
                text run at (2,2) width 74: "col 1 row 2"
            RenderTableCell {TD} at (504,30) size 78x26 [border: (1px inset #808080)] [r=1 c=1 rs=1 cs=1]
              RenderText {#text} at (2,2) size 74x22
                text run at (2,2) width 74: "col 2 row 2"
            RenderTableCell {TD} at (584,30) size 78x26 [border: (1px inset #808080)] [r=1 c=2 rs=1 cs=1]
              RenderText {#text} at (2,2) size 74x22
                text run at (2,2) width 74: "col 3 row 3"
      RenderBlock (anonymous) at (0,60) size 784x37
        RenderButton {BUTTON} at (2,2) size 470x33 [bgcolor=#C0C0C0]
          RenderBlock (anonymous) at (6,6) size 458x21
            RenderText {#text} at (0,0) size 458x21
              text run at (0,0) width 458: "Click me to test manually. The first column should grow to 500px."
        RenderText {#text} at (0,0) size 0x0
        RenderText {#text} at (0,0) size 0x0
