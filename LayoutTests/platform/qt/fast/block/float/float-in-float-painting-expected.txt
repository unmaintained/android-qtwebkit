layer at (0,0) size 800x600
  RenderView at (0,0) size 800x600
layer at (0,0) size 800x600
  RenderBlock {HTML} at (0,0) size 800x600
    RenderBody {BODY} at (8,8) size 784x584
      RenderBlock {DIV} at (0,0) size 784x66
        RenderText {#text} at (0,0) size 265x22
          text run at (0,0) width 265: "This test checks for regressions against "
        RenderInline {A} at (0,0) size 750x44 [color=#0000EE]
          RenderText {#text} at (265,0) size 750x44
            text run at (265,0) width 485: "Radar 4279765: REGRESSION: \"More...\" links on flickr groups pages"
            text run at (0,22) width 199: "have hover issues (flickr.com)"
        RenderText {#text} at (199,22) size 731x44
          text run at (199,22) width 532: ", or rather, a related painting issue. The word \"PASS\" should appear below in"
          text run at (0,44) width 113: "translucent blue."
layer at (8,74) size 784x125
  RenderBlock {DIV} at (0,66) size 784x125
    RenderBlock (floating) {DIV} at (0,0) size 104x125
      RenderImage {IMG} at (0,0) size 100x100
      RenderText {#text} at (100,84) size 4x22
        text run at (100,84) width 4: " "
      RenderBR {BR} at (0,0) size 0x0
      RenderBlock (floating) {H1} at (25,127) size 79x42
        RenderInline {A} at (0,0) size 79x42 [color=#0000EE]
          RenderText {#text} at (0,0) size 79x42
            text run at (0,0) width 79: "PASS"
      RenderText {#text} at (0,0) size 0x0
    RenderBR {BR} at (104,0) size 0x22
