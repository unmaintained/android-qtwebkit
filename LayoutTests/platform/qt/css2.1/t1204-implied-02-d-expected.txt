layer at (0,0) size 800x600
  RenderView at (0,0) size 800x600
layer at (0,0) size 800x106
  RenderBlock {HTML} at (0,0) size 800x106
    RenderBody {BODY} at (8,16) size 784x82
      RenderBlock {P} at (0,0) size 784x22
        RenderText {#text} at (0,0) size 291x22
          text run at (0,0) width 291: "The following two lines should be identical:"
      RenderBlock {DIV} at (0,38) size 784x22
        RenderInline {SPAN} at (0,0) size 35x22
          RenderInline (generated) at (0,0) size 11x22
            RenderText at (0,0) size 11x22
              text run at (0,0) width 11: "B"
          RenderInline {SPAN} at (0,0) size 12x22
            RenderInline (generated) at (0,0) size 12x22
              RenderCounter at (11,0) size 8x22
                text run at (11,0) width 8: "1"
              RenderText at (19,0) size 4x22
                text run at (19,0) width 4: " "
          RenderInline {SPAN} at (0,0) size 12x22
            RenderInline {SPAN} at (0,0) size 12x22
              RenderInline (generated) at (0,0) size 12x22
                RenderCounter at (23,0) size 8x22
                  text run at (23,0) width 8: "0"
                RenderText at (31,0) size 4x22
                  text run at (31,0) width 4: " "
        RenderInline {SPAN} at (0,0) size 32x22
          RenderInline {SPAN} at (0,0) size 12x22
            RenderInline (generated) at (0,0) size 12x22
              RenderCounter at (35,0) size 8x22
                text run at (35,0) width 8: "1"
              RenderText at (43,0) size 4x22
                text run at (43,0) width 4: " "
          RenderInline {SPAN} at (0,0) size 20x22
            RenderInline {SPAN} at (0,0) size 20x22
              RenderInline (generated) at (0,0) size 20x22
                RenderCounter at (47,0) size 20x22
                  text run at (47,0) width 20: "1.0"
                RenderText at (0,0) size 0x0
      RenderBlock {DIV} at (0,60) size 784x22
        RenderText {#text} at (0,0) size 67x22
          text run at (0,0) width 67: "B1 0 1 1.0"
