layer at (0,0) size 800x600
  RenderView at (0,0) size 800x600
layer at (0,0) size 800x168
  RenderBlock {HTML} at (0,0) size 800x168
    RenderBody {BODY} at (8,16) size 784x136
      RenderBlock {P} at (0,0) size 784x22
        RenderText {#text} at (0,0) size 363x22
          text run at (0,0) width 363: "The following three paragraphs should look identical:"
      RenderBlock {P} at (0,38) size 784x22
        RenderText {#text} at (0,0) size 164x22
          text run at (0,0) width 164: "This is a test paragraph."
      RenderBlock {P} at (0,76) size 784x22
        RenderInline (generated) at (0,0) size 34x22
          RenderText at (0,0) size 34x22
            text run at (0,0) width 34: "This "
        RenderText {#text} at (34,0) size 130x22
          text run at (34,0) width 130: "is a test paragraph."
      RenderBlock {P} at (0,114) size 784x22
        RenderInline (generated) at (0,0) size 34x22
          RenderText at (0,0) size 34x22
            text run at (0,0) width 34: "This "
        RenderText {#text} at (34,0) size 130x22
          text run at (34,0) width 130: "is a test paragraph."
