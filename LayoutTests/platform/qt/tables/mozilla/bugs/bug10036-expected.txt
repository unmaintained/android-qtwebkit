layer at (0,0) size 800x600
  RenderView at (0,0) size 800x600
layer at (0,0) size 800x600
  RenderBlock {HTML} at (0,0) size 800x600
    RenderBody {BODY} at (8,8) size 784x584
      RenderBlock (anonymous) at (0,0) size 784x22
        RenderText {#text} at (0,0) size 237x22
          text run at (0,0) width 237: "the lower left cell should be colored"
      RenderTable {TABLE} at (0,22) size 33x60 [border: (1px outset #808080)]
        RenderTableSection {TBODY} at (1,1) size 31x58
          RenderTableRow {TR} at (0,2) size 31x26
            RenderTableCell {TD} at (2,2) size 12x26 [bgcolor=#FFFF00] [border: (1px inset #808080)] [r=0 c=0 rs=1 cs=1]
              RenderText {#text} at (2,2) size 8x22
                text run at (2,2) width 8: "a"
            RenderTableCell {TD} at (16,2) size 13x26 [bgcolor=#00FFFF] [border: (1px inset #808080)] [r=0 c=1 rs=1 cs=1]
              RenderText {#text} at (2,2) size 9x22
                text run at (2,2) width 9: "b"
          RenderTableRow {TR} at (0,30) size 31x26
            RenderTableCell {TD} at (2,30) size 12x26 [bgcolor=#FF00FF] [border: (1px inset #808080)] [r=1 c=0 rs=1 cs=1]
              RenderBR {BR} at (2,2) size 0x22
            RenderTableCell {TD} at (16,30) size 13x26 [bgcolor=#088880] [border: (1px inset #808080)] [r=1 c=1 rs=1 cs=1]
              RenderText {#text} at (2,2) size 7x22
                text run at (2,2) width 7: "c"
