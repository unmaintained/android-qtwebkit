layer at (0,0) size 800x600
  RenderView at (0,0) size 800x600
layer at (0,0) size 800x600
  RenderSVGRoot {svg} at (45,0) size 270x70
    RenderSVGContainer {g} at (45,0) size 270x70
      RenderSVGPath {svg:line} at (45,0) size 10x70 [stroke={[type=SOLID] [color=#008000]}] [fill={[type=SOLID] [color=#000000]}] [x1=10.00] [y1=0.40] [x2=10.00] [y2=13.40]
      RenderSVGPath {svg:line} at (305,0) size 10x70 [stroke={[type=SOLID] [color=#008000]}] [fill={[type=SOLID] [color=#000000]}] [x1=61.75] [y1=0.40] [x2=61.75] [y2=13.40]
      RenderSVGPath {svg:rect} at (50,0) size 260x65 [fill={[type=SOLID] [color=#FF0000] [opacity=0.40]}] [x=10.00] [y=0.40] [width=51.75] [height=12.40]
    RenderSVGText {text} at (10,0) size 52x13 contains 1 chunk(s)
      RenderSVGInlineText {#text} at (0,0) size 52x13
        chunk 1 text run 1 at (10.00,10.00) startOffset 0 endOffset 1 width 6.20: "T"
        chunk 1 text run 2 at (14.45,10.00) startOffset 1 endOffset 2 width 5.20: "e"
        chunk 1 text run 3 at (17.91,10.00) startOffset 2 endOffset 3 width 5.60: "x"
        chunk 1 text run 4 at (21.76,10.00) startOffset 3 endOffset 4 width 3.80: "t"
        chunk 1 text run 5 at (23.81,10.00) startOffset 4 endOffset 5 width 3.20: " "
        chunk 1 text run 6 at (25.27,10.00) startOffset 5 endOffset 6 width 3.80: "t"
        chunk 1 text run 7 at (27.32,10.00) startOffset 6 endOffset 7 width 5.60: "o"
        chunk 1 text run 8 at (31.17,10.00) startOffset 7 endOffset 8 width 3.20: " "
        chunk 1 text run 9 at (32.63,10.00) startOffset 8 endOffset 9 width 6.60: "S"
        chunk 1 text run 10 at (37.48,10.00) startOffset 9 endOffset 10 width 5.80: "q"
        chunk 1 text run 11 at (41.53,10.00) startOffset 10 endOffset 11 width 6.80: "u"
        chunk 1 text run 12 at (46.59,10.00) startOffset 11 endOffset 12 width 5.20: "e"
        chunk 1 text run 13 at (50.04,10.00) startOffset 12 endOffset 13 width 5.20: "e"
        chunk 1 text run 14 at (53.49,10.00) startOffset 13 endOffset 14 width 4.80: "z"
        chunk 1 text run 15 at (56.55,10.00) startOffset 14 endOffset 15 width 5.20: "e"
selection start: position 0 of child 0 {#text} of child 5 {text} of child 0 {svg} of document
selection end:   position 15 of child 0 {#text} of child 5 {text} of child 0 {svg} of document
