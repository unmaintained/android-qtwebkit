layer at (0,0) size 800x600
  RenderView at (0,0) size 800x600
layer at (0,0) size 800x600
  RenderSVGRoot {svg} at (0,0) size 800x600
    RenderSVGContainer {g} at (28,6) size 772x493
      RenderSVGText {text} at (25,4) size 478x21 contains 1 chunk(s)
        RenderSVGInlineText {#text} at (0,0) size 478x21
          chunk 1 text run 1 at (25.00,20.00) startOffset 0 endOffset 60 width 477.60: "Basics of tspan: changing visual properties and positioning."
      RenderSVGContainer {g} at (78,78) size 576x141
        RenderSVGText {text} at (74,47) size 181x23 contains 1 chunk(s)
          RenderSVGInlineText {#text} at (0,1) size 61x21
            chunk 1 text run 1 at (74.00,63.75) startOffset 0 endOffset 7 width 60.60: "You are"
          RenderSVGTSpan {tspan} at (0,0) size 40x23
            RenderSVGInlineText {#text} at (60,0) size 40x23
              chunk 1 text run 1 at (134.60,63.75) startOffset 0 endOffset 5 width 39.00: " not "
          RenderSVGInlineText {#text} at (99,1) size 82x21
            chunk 1 text run 1 at (173.60,63.75) startOffset 0 endOffset 9 width 81.00: "a banana."
        RenderSVGPath {rect} at (78,121) size 521x94 [stroke={[type=SOLID] [color=#000000]}] [x=47.50] [y=74.25] [width=310.50] [height=53.50]
        RenderSVGText {text} at (65,75) size 235x21 contains 1 chunk(s)
          RenderSVGInlineText {#text} at (0,0) size 235x21
            chunk 1 text run 1 at (65.25,90.75) startOffset 0 endOffset 29 width 234.60: "Text: \"You are not a banana.\""
        RenderSVGText {text} at (65,92) size 327x21 contains 1 chunk(s)
          RenderSVGInlineText {#text} at (0,0) size 327x21
            chunk 1 text run 1 at (65.25,108.00) startOffset 0 endOffset 43 width 326.40: "'tspan' changes visual attributes of \"not\","
        RenderSVGText {text} at (65,109) size 97x22 contains 1 chunk(s)
          RenderSVGInlineText {#text} at (0,0) size 97x22
            chunk 1 text run 1 at (65.25,125.25) startOffset 0 endOffset 13 width 96.60: "to red, bold."
      RenderSVGContainer {g} at (373,210) size 427x182
        RenderSVGText {text} at (257,126) size 195x48 contains 1 chunk(s)
          RenderSVGInlineText {#text} at (0,12) size 64x21
            chunk 1 text run 1 at (257.50,153.75) startOffset 0 endOffset 7 width 63.00: "But you"
          RenderSVGTSpan {tspan} at (0,0) size 27x23
            RenderSVGInlineText {#text} at (95,0) size 27x23
              chunk 1 text run 1 at (352.50,143.00) startOffset 0 endOffset 3 width 26.40: "are"
          RenderSVGTSpan {tspan} at (0,0) size 74x22
            RenderSVGInlineText {#text} at (121,26) size 74x22
              chunk 1 text run 1 at (378.90,168.50) startOffset 0 endOffset 9 width 72.60: " a peach!"
        RenderSVGPath {rect} at (373,296) size 412x93 [stroke={[type=SOLID] [color=#000000]}] [x=225.00] [y=179.00] [width=245.50] [height=53.50]
        RenderSVGText {text} at (238,179) size 223x21 contains 1 chunk(s)
          RenderSVGInlineText {#text} at (0,0) size 223x21
            chunk 1 text run 1 at (238.00,195.00) startOffset 0 endOffset 28 width 222.60: "Text: \"But you are a peach!\""
        RenderSVGText {text} at (238,196) size 258x22 contains 1 chunk(s)
          RenderSVGInlineText {#text} at (0,0) size 258x22
            chunk 1 text run 1 at (238.00,212.25) startOffset 0 endOffset 34 width 258.00: "Using dx,dy, 'tspan' raises \"are\","
        RenderSVGText {text} at (238,213) size 193x22 contains 1 chunk(s)
          RenderSVGInlineText {#text} at (0,0) size 193x22
            chunk 1 text run 1 at (238.00,229.50) startOffset 0 endOffset 25 width 192.60: "'tspan' lowers \"a peach!\""
      RenderSVGContainer {g} at (28,306) size 624x193
        RenderSVGText {text} at (35,184) size 147x52 contains 1 chunk(s)
          RenderSVGTSpan {tspan} at (0,0) size 147x21
            RenderSVGInlineText {#text} at (0,0) size 147x21
              chunk 1 text run 1 at (35.00,200.00) startOffset 0 endOffset 1 width 12.00: "C"
              chunk 1 text run 1 at (53.75,200.00) startOffset 0 endOffset 1 width 10.80: "u"
              chunk 1 text run 1 at (72.50,200.00) startOffset 0 endOffset 1 width 6.00: "t"
              chunk 1 text run 1 at (91.25,200.00) startOffset 0 endOffset 1 width 8.40: "e"
              chunk 1 text run 1 at (110.50,200.00) startOffset 0 endOffset 1 width 5.40: " "
              chunk 1 text run 1 at (128.75,200.00) startOffset 0 endOffset 1 width 9.60: "a"
              chunk 1 text run 1 at (147.50,200.00) startOffset 0 endOffset 1 width 10.80: "n"
              chunk 1 text run 1 at (166.25,200.00) startOffset 0 endOffset 2 width 15.60: "d "
          RenderSVGInlineText {#text} at (0,0) size 0x0
          RenderSVGTSpan {tspan} at (0,0) size 90x22
            RenderSVGInlineText {#text} at (28,30) size 90x22
              chunk 1 text run 1 at (63.13,230.50) startOffset 0 endOffset 1 width 5.40: "f"
              chunk 1 text run 1 at (81.88,230.50) startOffset 0 endOffset 1 width 10.80: "u"
              chunk 1 text run 1 at (100.63,230.50) startOffset 0 endOffset 1 width 7.80: "z"
              chunk 1 text run 1 at (119.38,230.50) startOffset 0 endOffset 1 width 7.80: "z"
              chunk 1 text run 1 at (138.13,230.50) startOffset 0 endOffset 2 width 14.40: "y."
          RenderSVGInlineText {#text} at (0,0) size 0x0
        RenderSVGPath {rect} at (28,406) size 562x93 [stroke={[type=SOLID] [color=#000000]}] [x=17.50] [y=244.75] [width=335.50] [height=53.50]
        RenderSVGText {text} at (25,242) size 182x21 contains 1 chunk(s)
          RenderSVGInlineText {#text} at (0,0) size 182x21
            chunk 1 text run 1 at (25.25,258.00) startOffset 0 endOffset 23 width 181.20: "Text: \"Cute and fuzzy.\""
        RenderSVGText {text} at (25,259) size 366x22 contains 1 chunk(s)
          RenderSVGInlineText {#text} at (0,0) size 366x22
            chunk 1 text run 1 at (25.25,275.25) startOffset 0 endOffset 45 width 364.80: "'tspan' char-by-char placement of \"Cute and\","
        RenderSVGText {text} at (25,276) size 298x22 contains 1 chunk(s)
          RenderSVGInlineText {#text} at (0,0) size 298x22
            chunk 1 text run 1 at (25.25,292.50) startOffset 0 endOffset 39 width 297.60: "'tspan' char-by-char \"fuzzy\", below it."
    RenderSVGText {text} at (10,301) size 274x53 contains 1 chunk(s)
      RenderSVGInlineText {#text} at (0,0) size 274x53
        chunk 1 text run 1 at (10.00,340.00) startOffset 0 endOffset 16 width 273.60: "$Revision: 1.7 $"
    RenderSVGPath {rect} at (0,0) size 800x600 [stroke={[type=SOLID] [color=#000000]}] [x=1.00] [y=1.00] [width=478.00] [height=358.00]
selection start: position 0 of child 0 {#text} of child 1 {text} of child 13 {g} of child 35 {svg} of document
selection end:   position 16 of child 0 {#text} of child 15 {text} of child 35 {svg} of document
