layer at (0,0) size 480x360
  RenderView at (0,0) size 480x360
layer at (0,0) size 480x360
  RenderSVGRoot {svg} at (0,0) size 480x360
    RenderSVGContainer {g} at (60,19) size 360x321
      RenderSVGText {text} at (360,19) size 51x39 contains 1 chunk(s)
        RenderSVGInlineText {#text} at (0,0) size 51x39
          chunk 1 text run 1 at (360.00,50.00) startOffset 0 endOffset 3 width 51.00: "100"
      RenderSVGText {text} at (360,54) size 51x39 contains 1 chunk(s)
        RenderSVGInlineText {#text} at (0,0) size 51x39
          chunk 1 text run 1 at (360.00,85.00) startOffset 0 endOffset 3 width 51.00: "200"
      RenderSVGText {text} at (360,91) size 57x38 contains 1 chunk(s)
        RenderSVGInlineText {#text} at (0,0) size 57x38
          chunk 1 text run 1 at (360.00,120.00) startOffset 0 endOffset 3 width 57.00: "300"
      RenderSVGText {text} at (360,126) size 57x38 contains 1 chunk(s)
        RenderSVGInlineText {#text} at (0,0) size 57x38
          chunk 1 text run 1 at (360.00,155.00) startOffset 0 endOffset 3 width 57.00: "400"
      RenderSVGText {text} at (360,161) size 57x38 contains 1 chunk(s)
        RenderSVGInlineText {#text} at (0,0) size 57x38
          chunk 1 text run 1 at (360.00,190.00) startOffset 0 endOffset 3 width 57.00: "500"
      RenderSVGText {text} at (360,193) size 51x40 contains 1 chunk(s)
        RenderSVGInlineText {#text} at (0,0) size 51x40
          chunk 1 text run 1 at (360.00,225.00) startOffset 0 endOffset 3 width 51.00: "600"
      RenderSVGText {text} at (360,230) size 60x40 contains 1 chunk(s)
        RenderSVGInlineText {#text} at (0,0) size 60x40
          chunk 1 text run 1 at (360.00,260.00) startOffset 0 endOffset 3 width 60.00: "700"
      RenderSVGText {text} at (360,265) size 60x40 contains 1 chunk(s)
        RenderSVGInlineText {#text} at (0,0) size 60x40
          chunk 1 text run 1 at (360.00,295.00) startOffset 0 endOffset 3 width 60.00: "800"
      RenderSVGText {text} at (360,300) size 60x40 contains 1 chunk(s)
        RenderSVGInlineText {#text} at (0,0) size 60x40
          chunk 1 text run 1 at (360.00,330.00) startOffset 0 endOffset 3 width 60.00: "900"
      RenderSVGText {text} at (60,50) size 181x40 contains 1 chunk(s)
        RenderSVGInlineText {#text} at (0,0) size 181x40
          chunk 1 text run 1 at (60.00,80.00) startOffset 0 endOffset 12 width 181.00: "This is bold"
      RenderSVGText {text} at (60,101) size 213x38 contains 1 chunk(s)
        RenderSVGInlineText {#text} at (0,0) size 213x38
          chunk 1 text run 1 at (60.00,130.00) startOffset 0 endOffset 14 width 213.00: "This is normal"
      RenderSVGContainer {g} at (60,150) size 213x40
        RenderSVGText {text} at (60,150) size 213x40 contains 1 chunk(s)
          RenderSVGInlineText {#text} at (0,0) size 213x40
            chunk 1 text run 1 at (60.00,180.00) startOffset 0 endOffset 14 width 213.00: "Blue is bolder"
      RenderSVGContainer {g} at (60,201) size 206x38
        RenderSVGText {text} at (60,201) size 206x38 contains 1 chunk(s)
          RenderSVGInlineText {#text} at (0,0) size 206x38
            chunk 1 text run 1 at (60.00,230.00) startOffset 0 endOffset 15 width 206.00: "Blue is lighter"
    RenderSVGText {text} at (10,301) size 270x53 contains 1 chunk(s)
      RenderSVGInlineText {#text} at (0,0) size 270x53
        chunk 1 text run 1 at (10.00,340.00) startOffset 0 endOffset 16 width 270.00: "$Revision: 1.7 $"
    RenderSVGPath {rect} at (0,0) size 480x360 [stroke={[type=SOLID] [color=#000000]}] [x=1.00] [y=1.00] [width=478.00] [height=358.00]
