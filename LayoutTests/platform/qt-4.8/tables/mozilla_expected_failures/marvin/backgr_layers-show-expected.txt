layer at (0,0) size 784x1732
  RenderView at (0,0) size 784x600
layer at (0,0) size 784x1732
  RenderBlock {HTML} at (0,0) size 784x1732
    RenderBody {BODY} at (8,17) size 768x1707 [color=#00FF00] [bgcolor=#333333]
      RenderBlock {H1} at (0,0) size 768x34
        RenderText {#text} at (0,0) size 483x34
          text run at (0,0) width 483: "CSS2 Table Backgrounds Test Suite"
      RenderBlock {H2} at (0,51) size 768x27
        RenderText {#text} at (0,0) size 278x27
          text run at (0,0) width 278: "Part C: Background Layers"
      RenderBlock {H3} at (0,94) size 768x20
        RenderText {#text} at (0,0) size 140x20
          text run at (0,0) width 140: "empty-cells: show"
      RenderBlock {P} at (0,129) size 768x17
        RenderText {#text} at (0,0) size 286x17
          text run at (0,0) width 286: "Both tables should have a blue background."
      RenderBlock {P} at (0,159) size 768x17
        RenderText {#text} at (0,0) size 367x17
          text run at (0,0) width 367: "In table cell C (third cell in the first row), which is empty:"
      RenderBlock {UL} at (0,189) size 768x103
        RenderListItem {LI} at (40,0) size 728x17
          RenderListMarker at (-16,0) size 6x17: bullet
          RenderText {#text} at (0,0) size 693x17
            text run at (0,0) width 693: "Four sets of horizontal double violet stripes surrounded by aqua should run just inside the top border edge."
        RenderListItem {LI} at (40,17) size 728x17
          RenderListMarker at (-16,0) size 6x17: bullet
          RenderText {#text} at (0,0) size 692x17
            text run at (0,0) width 692: "One set of aqua-backed double violet stripes should run just inside the left, right, and bottom border edges."
        RenderListItem {LI} at (40,34) size 728x34
          RenderListMarker at (-16,0) size 6x17: bullet
          RenderText {#text} at (0,0) size 725x34
            text run at (0,0) width 725: "The third set along the top should turn down at the right edge and go under the fourth set to form the right-edge"
            text run at (0,17) width 23: "set."
        RenderListItem {LI} at (40,68) size 728x17
          RenderListMarker at (-16,0) size 6x17: bullet
          RenderText {#text} at (0,0) size 434x17
            text run at (0,0) width 434: "The fourth set should turn down at the left edge to form the left set."
        RenderListItem {LI} at (40,85) size 728x18
          RenderListMarker at (-16,0) size 6x17: bullet
          RenderText {#text} at (0,0) size 339x17
            text run at (0,0) width 339: "The bottom stripe should be straight and cut across "
          RenderInline {EM} at (0,0) size 24x18
            RenderText {#text} at (339,0) size 24x18
              text run at (339,0) width 24: "over"
          RenderText {#text} at (363,0) size 89x17
            text run at (363,0) width 89: " the side sets."
      RenderBlock {P} at (0,305) size 768x17
        RenderText {#text} at (0,0) size 259x17
          text run at (0,0) width 259: "In table cell A, (first cell in the first row):"
      RenderBlock {UL} at (0,335) size 768x68
        RenderListItem {LI} at (40,0) size 728x17
          RenderListMarker at (-16,0) size 6x17: bullet
          RenderText {#text} at (0,0) size 652x17
            text run at (0,0) width 652: "Three sets of horizontal aqua-backed double violet stripes should run just inside the top border edge."
        RenderListItem {LI} at (40,17) size 728x17
          RenderListMarker at (-16,0) size 6x17: bullet
          RenderText {#text} at (0,0) size 203x17
            text run at (0,0) width 203: "The first set should run across."
        RenderListItem {LI} at (40,34) size 728x34
          RenderListMarker at (-16,0) size 6x17: bullet
          RenderText {#text} at (0,0) size 693x34
            text run at (0,0) width 693: "The second set should turn down at the left edge, going over the third set to form another set that runs just"
            text run at (0,17) width 171: "inside the left border edge."
      RenderBlock {P} at (0,416) size 768x17
        RenderText {#text} at (0,0) size 258x17
          text run at (0,0) width 258: "In table cell D, (last cell in the first row):"
      RenderBlock {UL} at (0,446) size 768x51
        RenderListItem {LI} at (40,0) size 728x17
          RenderListMarker at (-16,0) size 6x17: bullet
          RenderText {#text} at (0,0) size 633x17
            text run at (0,0) width 633: "Two sets of horizontal aqua-backed double violet strips should run just inside the top border edge."
        RenderListItem {LI} at (40,17) size 728x34
          RenderListMarker at (-16,0) size 6x17: bullet
          RenderText {#text} at (0,0) size 693x34
            text run at (0,0) width 693: "The first set should turn down at the right edge, going under the second horizontal set to run vertically just"
            text run at (0,17) width 182: "inside the right border edge."
      RenderBlock {P} at (0,510) size 768x17
        RenderText {#text} at (0,0) size 285x17
          text run at (0,0) width 285: "In table cell G, (third cell in the second row):"
      RenderBlock {UL} at (0,540) size 768x68
        RenderListItem {LI} at (40,0) size 728x17
          RenderListMarker at (-16,0) size 6x17: bullet
          RenderText {#text} at (0,0) size 640x17
            text run at (0,0) width 640: "Two sets of horizontal aqua-backed double violet stripes should run just inside the top border edge."
        RenderListItem {LI} at (40,17) size 728x17
          RenderListMarker at (-16,0) size 6x17: bullet
          RenderText {#text} at (0,0) size 700x17
            text run at (0,0) width 700: "A set of vertical stripes should run down just inside the left border edge, going under both horizontal stripes."
        RenderListItem {LI} at (40,34) size 728x34
          RenderListMarker at (-16,0) size 6x17: bullet
          RenderText {#text} at (0,0) size 663x34
            text run at (0,0) width 663: "Another set of vertical stripes should run down just inside the right border edge, also going under both"
            text run at (0,17) width 117: "horizontal stripes."
      RenderBlock {DL} at (0,621) size 768x102
        RenderBlock {DT} at (0,0) size 768x17
          RenderText {#text} at (0,0) size 28x17
            text run at (0,0) width 28: "next"
        RenderBlock {DD} at (40,17) size 728x17
          RenderInline {A} at (0,0) size 248x17 [color=#FFFF00]
            RenderText {#text} at (0,0) size 248x17
              text run at (0,0) width 248: "Background Layers: empty-cells: show"
          RenderText {#text} at (0,0) size 0x0
        RenderBlock {DT} at (0,34) size 768x17
          RenderText {#text} at (0,0) size 55x17
            text run at (0,0) width 55: "previous"
        RenderBlock {DD} at (40,51) size 728x17
          RenderInline {A} at (0,0) size 310x17 [color=#FFFF00]
            RenderText {#text} at (0,0) size 310x17
              text run at (0,0) width 310: "Background Position: Background on 'table-cell'"
          RenderText {#text} at (0,0) size 0x0
        RenderBlock {DT} at (0,68) size 768x17
          RenderText {#text} at (0,0) size 56x17
            text run at (0,0) width 56: "contents"
        RenderBlock {DD} at (40,85) size 728x17
          RenderInline {A} at (0,0) size 113x17 [color=#FFFF00]
            RenderText {#text} at (0,0) size 113x17
              text run at (0,0) width 113: "Table of Contents"
          RenderText {#text} at (0,0) size 0x0
      RenderTable {TABLE} at (0,736) size 618x483 [color=#FFFFFF] [bgcolor=#0000FF] [border: (1px dotted #FFFFFF)]
        RenderBlock {CAPTION} at (0,0) size 616x25
          RenderText {#text} at (162,0) size 292x25
            text run at (162,0) width 292: "With 'border-collapse: separate'"
        RenderTableCol {COLGROUP} at (0,0) size 0x0
          RenderTableCol {COL} at (0,0) size 0x0
          RenderTableCol {COL} at (0,0) size 0x0
          RenderTableCol {COL} at (0,0) size 0x0
        RenderTableCol {COLGROUP} at (0,0) size 0x0
          RenderTableCol {COL} at (0,0) size 0x0
        RenderTableSection {THEAD} at (1,26) size 616x114
          RenderTableRow {TR} at (0,7) size 616x100
            RenderTableCell {TH} at (7,42) size 130x29 [border: (1px dotted #FFFFFF)] [r=0 c=0 rs=1 cs=1]
              RenderText {#text} at (42,2) size 46x25
                text run at (42,2) width 46: "TH A"
            RenderTableCell {TH} at (144,30) size 220x53 [border: (13px dotted #FFFFFF)] [r=0 c=1 rs=1 cs=1]
              RenderText {#text} at (87,14) size 46x25
                text run at (87,14) width 46: "TH B"
            RenderTableCell {TH} at (371,55) size 118x4 [border: (1px dotted #FFFFFF)] [r=0 c=2 rs=1 cs=1]
            RenderTableCell {TH} at (496,42) size 113x29 [border: (1px dotted #FFFFFF)] [r=0 c=3 rs=1 cs=1]
              RenderText {#text} at (34,2) size 45x25
                text run at (34,2) width 45: "TH D"
        RenderTableSection {TFOOT} at (1,353) size 616x129
          RenderTableRow {TR} at (0,7) size 616x115
            RenderTableCell {TD} at (7,50) size 357x29 [border: (1px dotted #FFFFFF)] [r=0 c=0 rs=1 cs=2]
              RenderText {#text} at (2,2) size 47x25
                text run at (2,2) width 47: "TD M"
            RenderTableCell {TD} at (371,50) size 118x29 [border: (1px dotted #FFFFFF)] [r=0 c=2 rs=1 cs=1]
              RenderText {#text} at (2,2) size 46x25
                text run at (2,2) width 46: "TD O"
            RenderTableCell {TD} at (496,50) size 113x29 [border: (1px dotted #FFFFFF)] [r=0 c=3 rs=1 cs=1]
              RenderText {#text} at (2,2) size 44x25
                text run at (2,2) width 44: "TD P"
        RenderTableSection {TBODY} at (1,140) size 616x213
          RenderTableRow {TR} at (0,7) size 616x108
            RenderTableCell {TD} at (7,80) size 130x53 [border: (13px dotted #FFFFFF)] [r=0 c=0 rs=2 cs=1]
              RenderText {#text} at (14,14) size 44x25
                text run at (14,14) width 44: "TD E"
            RenderTableCell {TD} at (144,46) size 220x29 [border: (1px dotted #FFFFFF)] [r=0 c=1 rs=1 cs=1]
              RenderText {#text} at (2,2) size 43x25
                text run at (2,2) width 43: "TD F"
            RenderTableCell {TD} at (371,46) size 118x29 [border: (1px dotted #FFFFFF)] [r=0 c=2 rs=1 cs=1]
              RenderText {#text} at (2,2) size 46x25
                text run at (2,2) width 46: "TD G"
            RenderTableCell {TD} at (496,46) size 113x29 [border: (1px dotted #FFFFFF)] [r=0 c=3 rs=1 cs=1]
              RenderText {#text} at (2,2) size 45x25
                text run at (2,2) width 45: "TD H"
          RenderTableRow {TR} at (0,122) size 616x84
            RenderTableCell {TD} at (144,149) size 220x29 [border: (1px dotted #FFFFFF)] [r=1 c=1 rs=1 cs=1]
              RenderText {#text} at (2,2) size 42x25
                text run at (2,2) width 42: "TD J"
            RenderTableCell {TD} at (371,149) size 118x29 [border: (1px dotted #FFFFFF)] [r=1 c=2 rs=1 cs=1]
              RenderText {#text} at (2,2) size 45x25
                text run at (2,2) width 45: "TD K"
            RenderTableCell {TD} at (496,149) size 113x29 [border: (1px dotted #FFFFFF)] [r=1 c=3 rs=1 cs=1]
              RenderText {#text} at (2,2) size 43x25
                text run at (2,2) width 43: "TD L"
      RenderTable {TABLE} at (0,1219) size 578x435 [color=#FFFFFF] [bgcolor=#0000FF] [border: (6px dotted #FFFFFF)]
        RenderBlock {CAPTION} at (0,0) size 577x25
          RenderText {#text} at (143,0) size 291x25
            text run at (143,0) width 291: "With 'border-collapse: collapse'"
        RenderTableCol {COLGROUP} at (0,0) size 0x0
          RenderTableCol {COL} at (0,0) size 0x0
          RenderTableCol {COL} at (0,0) size 0x0
          RenderTableCol {COL} at (0,0) size 0x0
        RenderTableCol {COLGROUP} at (0,0) size 0x0
          RenderTableCol {COL} at (0,0) size 0x0
        RenderTableSection {THEAD} at (0,31) size 577x99
          RenderTableRow {TR} at (0,0) size 577x99
            RenderTableCell {TH} at (0,32) size 135x34 [border: (1px dotted #FFFFFF)] [r=0 c=0 rs=1 cs=1]
              RenderText {#text} at (42,2) size 46x25
                text run at (42,2) width 46: "TH A"
            RenderTableCell {TH} at (135,29) size 207x40 [border: (7px dotted #FFFFFF)] [r=0 c=1 rs=1 cs=1]
              RenderText {#text} at (81,8) size 46x25
                text run at (81,8) width 46: "TH B"
            RenderTableCell {TH} at (342,48) size 123x3 [border: (1px dotted #FFFFFF)] [r=0 c=2 rs=1 cs=1]
            RenderTableCell {TH} at (465,35) size 112x28 [border: (1px dotted #FFFFFF)] [r=0 c=3 rs=1 cs=1]
              RenderText {#text} at (34,2) size 45x25
                text run at (34,2) width 45: "TH D"
        RenderTableSection {TFOOT} at (0,320) size 577x114
          RenderTableRow {TR} at (0,0) size 577x114
            RenderTableCell {TD} at (0,40) size 342x34 [border: (7px dotted #FFFFFF)] [r=0 c=0 rs=1 cs=2]
              RenderText {#text} at (2,8) size 47x25
                text run at (2,8) width 47: "TD M"
            RenderTableCell {TD} at (342,43) size 123x28 [border: (1px dotted #FFFFFF)] [r=0 c=2 rs=1 cs=1]
              RenderText {#text} at (2,2) size 46x25
                text run at (2,2) width 46: "TD O"
            RenderTableCell {TD} at (465,43) size 112x28 [border: (1px dotted #FFFFFF)] [r=0 c=3 rs=1 cs=1]
              RenderText {#text} at (2,2) size 44x25
                text run at (2,2) width 44: "TD P"
        RenderTableSection {TBODY} at (0,130) size 577x190
          RenderTableRow {TR} at (0,0) size 577x107
            RenderTableCell {TD} at (0,75) size 135x40 [border: (7px dotted #FFFFFF)] [r=0 c=0 rs=2 cs=1]
              RenderText {#text} at (8,8) size 44x25
                text run at (8,8) width 44: "TD E"
            RenderTableCell {TD} at (135,36) size 207x34 [border: (7px dotted #FFFFFF)] [r=0 c=1 rs=1 cs=1]
              RenderText {#text} at (8,8) size 43x25
                text run at (8,8) width 43: "TD F"
            RenderTableCell {TD} at (342,39) size 123x28 [border: (1px dotted #FFFFFF)] [r=0 c=2 rs=1 cs=1]
              RenderText {#text} at (2,2) size 46x25
                text run at (2,2) width 46: "TD G"
            RenderTableCell {TD} at (465,39) size 112x28 [border: (1px dotted #FFFFFF)] [r=0 c=3 rs=1 cs=1]
              RenderText {#text} at (2,2) size 45x25
                text run at (2,2) width 45: "TD H"
          RenderTableRow {TR} at (0,107) size 577x83
            RenderTableCell {TD} at (135,134) size 207x28 [border: (1px dotted #FFFFFF)] [r=1 c=1 rs=1 cs=1]
              RenderText {#text} at (8,2) size 42x25
                text run at (8,2) width 42: "TD J"
            RenderTableCell {TD} at (342,134) size 123x28 [border: (1px dotted #FFFFFF)] [r=1 c=2 rs=1 cs=1]
              RenderText {#text} at (2,2) size 45x25
                text run at (2,2) width 45: "TD K"
            RenderTableCell {TD} at (465,134) size 112x28 [border: (1px dotted #FFFFFF)] [r=1 c=3 rs=1 cs=1]
              RenderText {#text} at (2,2) size 43x25
                text run at (2,2) width 43: "TD L"
      RenderBlock {DIV} at (0,1654) size 768x35
        RenderInline {A} at (0,0) size 88x17 [color=#FFFF00]
          RenderImage {IMG} at (0,0) size 88x31
        RenderText {#text} at (0,0) size 0x0
      RenderBlock {ADDRESS} at (0,1689) size 768x18
        RenderText {#text} at (0,0) size 517x18
          text run at (0,0) width 517: "CSS2 Table Backgrounds Test Suite designed and written by fantasai <fantasai@escape.com>"
