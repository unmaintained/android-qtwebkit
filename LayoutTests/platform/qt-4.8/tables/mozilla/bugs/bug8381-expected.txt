layer at (0,0) size 800x600
  RenderView at (0,0) size 800x600
layer at (0,0) size 800x600
  RenderBlock {HTML} at (0,0) size 800x600
    RenderBody {BODY} at (8,8) size 784x584 [bgcolor=#3333FF]
      RenderBlock {CENTER} at (0,0) size 784x78
        RenderTable {TABLE} at (181,0) size 421x78 [bgcolor=#FFFFFF] [border: (2px outset #808080)]
          RenderBlock {CAPTION} at (0,0) size 417x24
            RenderInline {B} at (0,0) size 141x22
              RenderInline {FONT} at (0,0) size 141x24 [color=#C0C0C0]
                RenderText {#text} at (138,0) size 141x24
                  text run at (138,0) width 141: "Future Bugzilla"
            RenderText {#text} at (0,0) size 0x0
          RenderTableSection {TBODY} at (2,26) size 417x50
            RenderTableRow {TR} at (0,2) size 417x22
              RenderTableCell {TD} at (2,2) size 40x22 [bgcolor=#009900] [border: (1px inset #808080)] [r=0 c=0 rs=1 cs=1]
                RenderBlock {DIV} at (2,2) size 36x18
                  RenderInline {B} at (0,0) size 36x22
                    RenderInline {FONT} at (0,0) size 36x18
                      RenderText {#text} at (0,0) size 36x18
                        text run at (0,0) width 36: "Credit"
              RenderTableCell {TD} at (44,2) size 39x22 [bgcolor=#009900] [border: (1px inset #808080)] [r=0 c=1 rs=1 cs=1]
                RenderInline {B} at (0,0) size 23x22
                  RenderInline {FONT} at (0,0) size 23x18
                    RenderText {#text} at (2,2) size 23x18
                      text run at (2,2) width 23: "Bug"
              RenderTableCell {TD} at (85,2) size 138x22 [bgcolor=#009900] [border: (1px inset #808080)] [r=0 c=2 rs=1 cs=1]
                RenderInline {B} at (0,0) size 56x22
                  RenderInline {FONT} at (0,0) size 56x18
                    RenderText {#text} at (2,2) size 56x18
                      text run at (2,2) width 56: "Summary"
              RenderTableCell {TD} at (225,2) size 55x22 [bgcolor=#009900] [border: (1px inset #808080)] [r=0 c=3 rs=1 cs=1]
                RenderInline {B} at (0,0) size 34x22
                  RenderInline {FONT} at (0,0) size 34x18
                    RenderText {#text} at (2,2) size 34x18
                      text run at (2,2) width 34: "Status"
              RenderTableCell {TD} at (282,2) size 64x22 [bgcolor=#009900] [border: (1px inset #808080)] [r=0 c=4 rs=1 cs=1]
                RenderInline {B} at (0,0) size 60x22
                  RenderInline {FONT} at (0,0) size 60x18
                    RenderText {#text} at (2,2) size 60x18
                      text run at (2,2) width 60: "Resolution"
              RenderTableCell {TD} at (348,2) size 67x22 [bgcolor=#009900] [border: (1px inset #808080)] [r=0 c=5 rs=1 cs=1]
                RenderInline {B} at (0,0) size 63x22
                  RenderInline {FONT} at (0,0) size 63x18
                    RenderText {#text} at (2,2) size 63x18
                      text run at (2,2) width 63: "Date (-8/-7)"
            RenderTableRow {TR} at (0,26) size 417x22
              RenderTableCell {TD} at (2,26) size 40x22 [bgcolor=#FF0000] [border: (1px inset #808080)] [r=1 c=0 rs=1 cs=1]
                RenderInline {B} at (0,0) size 7x22
                  RenderInline {FONT} at (0,0) size 7x18
                    RenderText {#text} at (2,2) size 7x18
                      text run at (2,2) width 7: "1"
              RenderTableCell {TD} at (44,26) size 39x22 [bgcolor=#FF0000] [border: (1px inset #808080)] [r=1 c=1 rs=1 cs=1]
                RenderInline {B} at (0,0) size 35x22
                  RenderInline {FONT} at (0,0) size 35x18
                    RenderText {#text} at (0,0) size 0x0
                    RenderInline {A} at (0,0) size 35x18
                      RenderText {#text} at (2,2) size 35x18
                        text run at (2,2) width 35: "10000"
                    RenderText {#text} at (0,0) size 0x0
              RenderTableCell {TD} at (85,26) size 138x22 [bgcolor=#FF0000] [border: (1px inset #808080)] [r=1 c=2 rs=1 cs=1]
                RenderInline {B} at (0,0) size 134x22
                  RenderInline {FONT} at (0,0) size 134x18
                    RenderText {#text} at (2,2) size 134x18
                      text run at (2,2) width 134: "JWZ returns to Mozilla"
              RenderTableCell {TD} at (225,26) size 55x22 [bgcolor=#FF0000] [border: (1px inset #808080)] [r=1 c=3 rs=1 cs=1]
                RenderInline {B} at (0,0) size 51x22
                  RenderInline {FONT} at (0,0) size 51x18
                    RenderText {#text} at (2,2) size 51x18
                      text run at (2,2) width 51: "Resolved"
              RenderTableCell {TD} at (282,26) size 64x22 [bgcolor=#FF0000] [border: (1px inset #808080)] [r=1 c=4 rs=1 cs=1]
                RenderInline {B} at (0,0) size 54x22
                  RenderInline {FONT} at (0,0) size 54x18
                    RenderText {#text} at (2,2) size 54x18
                      text run at (2,2) width 54: "Duplicate"
              RenderTableCell {TD} at (348,26) size 67x22 [bgcolor=#FF0000] [border: (1px inset #808080)] [r=1 c=5 rs=1 cs=1]
                RenderInline {B} at (0,0) size 35x22
                  RenderInline {FONT} at (0,0) size 35x18
                    RenderText {#text} at (2,2) size 35x18
                      text run at (2,2) width 35: "July 1"
