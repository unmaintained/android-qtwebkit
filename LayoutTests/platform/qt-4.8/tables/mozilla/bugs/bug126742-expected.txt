layer at (0,0) size 800x600
  RenderView at (0,0) size 800x600
layer at (0,0) size 800x600
  RenderBlock {HTML} at (0,0) size 800x600
    RenderBody {BODY} at (8,8) size 784x584
      RenderBlock {CENTER} at (0,0) size 784x376
        RenderTable {TABLE} at (0,0) size 784x376
          RenderTableSection {TBODY} at (0,0) size 784x376
            RenderTableRow {TR} at (0,0) size 784x66
              RenderTableCell {TD} at (0,7) size 784x51 [r=0 c=0 rs=1 cs=1]
                RenderBlock {H2} at (0,0) size 784x32
                  RenderInline {FONT} at (0,0) size 108x32
                    RenderText {#text} at (338,0) size 108x32
                      text run at (338,0) width 108: "EET 304"
            RenderTableRow {TR} at (0,66) size 784x22
              RenderTableCell {TD} at (0,68) size 784x17 [r=1 c=0 rs=1 cs=1]
                RenderBlock {P} at (0,0) size 784x17
                  RenderInline {FONT} at (0,0) size 36x17
                    RenderInline {A} at (0,0) size 36x17 [color=#0000EE]
                      RenderText {#text} at (374,0) size 36x17
                        text run at (374,0) width 36: "Home"
            RenderTableRow {TR} at (0,88) size 784x32
              RenderTableCell {TD} at (0,95) size 784x17 [r=2 c=0 rs=1 cs=1]
                RenderInline {A} at (0,0) size 126x22 [color=#0000EE]
                  RenderInline {FONT} at (0,0) size 126x17
                    RenderText {#text} at (329,0) size 126x17
                      text run at (329,0) width 126: "Course Information"
            RenderTableRow {TR} at (0,120) size 784x32
              RenderTableCell {TD} at (0,127) size 784x17 [r=3 c=0 rs=1 cs=1]
                RenderInline {A} at (0,0) size 50x22 [color=#0000EE]
                  RenderInline {FONT} at (0,0) size 50x17
                    RenderText {#text} at (367,0) size 50x17
                      text run at (367,0) width 50: "Lecture"
            RenderTableRow {TR} at (0,152) size 784x32
              RenderTableCell {TD} at (0,159) size 784x17 [r=4 c=0 rs=1 cs=1]
                RenderInline {A} at (0,0) size 0x0 [color=#0000EE]
                  RenderInline {FONT} at (0,0) size 0x0
                RenderInline {A} at (0,0) size 68x22 [color=#0000EE]
                  RenderInline {FONT} at (0,0) size 68x17
                    RenderText {#text} at (358,0) size 68x17
                      text run at (358,0) width 68: "Homework"
            RenderTableRow {TR} at (0,184) size 784x32
              RenderTableCell {TD} at (0,191) size 784x17 [r=5 c=0 rs=1 cs=1]
                RenderInline {A} at (0,0) size 50x22 [color=#0000EE]
                  RenderInline {FONT} at (0,0) size 50x17
                    RenderText {#text} at (367,0) size 50x17
                      text run at (367,0) width 50: "Quizzes"
            RenderTableRow {TR} at (0,216) size 784x32
              RenderTableCell {TD} at (0,223) size 784x17 [r=6 c=0 rs=1 cs=1]
                RenderInline {FONT} at (0,0) size 44x17 [color=#0000FF]
                  RenderInline {A} at (0,0) size 44x17 [color=#0000EE]
                    RenderText {#text} at (370,0) size 44x17
                      text run at (370,0) width 44: "Exams"
            RenderTableRow {TR} at (0,248) size 784x32
              RenderTableCell {TD} at (0,255) size 784x17 [r=7 c=0 rs=1 cs=1]
                RenderInline {A} at (0,0) size 0x0 [color=#0000EE]
                  RenderInline {FONT} at (0,0) size 0x0
                RenderInline {A} at (0,0) size 70x22 [color=#0000EE]
                  RenderInline {FONT} at (0,0) size 70x17
                    RenderText {#text} at (357,0) size 24x17
                      text run at (357,0) width 24: "Lab"
                    RenderInline {U} at (0,0) size 46x17
                      RenderText {#text} at (381,0) size 46x17
                        text run at (381,0) width 46: "oratory"
            RenderTableRow {TR} at (0,280) size 784x32
              RenderTableCell {TD} at (0,287) size 784x17 [r=8 c=0 rs=1 cs=1]
                RenderInline {A} at (0,0) size 46x22 [color=#0000EE]
                  RenderInline {U} at (0,0) size 46x22
                    RenderInline {FONT} at (0,0) size 46x17
                      RenderText {#text} at (369,0) size 46x17
                        text run at (369,0) width 46: "Grades"
            RenderTableRow {TR} at (0,312) size 784x32
              RenderTableCell {TD} at (0,319) size 784x17 [r=9 c=0 rs=1 cs=1]
                RenderInline {FONT} at (0,0) size 82x17
                  RenderInline {A} at (0,0) size 82x17 [color=#0000EE]
                    RenderText {#text} at (351,0) size 82x17
                      text run at (351,0) width 38: "Filter "
                      text run at (389,0) width 44: "Design"
            RenderTableRow {TR} at (0,344) size 784x32
              RenderTableCell {TD} at (0,351) size 784x17 [r=10 c=0 rs=1 cs=1]
                RenderInline {FONT} at (0,0) size 80x17
                  RenderInline {U} at (0,0) size 80x17
                    RenderInline {A} at (0,0) size 80x17 [color=#0000EE]
                      RenderText {#text} at (352,0) size 80x17
                        text run at (352,0) width 41: "Office "
                        text run at (392,0) width 40: "Hours"
