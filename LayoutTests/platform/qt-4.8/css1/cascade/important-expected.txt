layer at (0,0) size 784x603
  RenderView at (0,0) size 784x600
layer at (0,0) size 784x603
  RenderBlock {HTML} at (0,0) size 784x603
    RenderBody {BODY} at (8,8) size 768x587 [bgcolor=#CCCCCC]
      RenderBlock {P} at (0,0) size 768x22 [color=#008000]
        RenderText {#text} at (0,0) size 380x22
          text run at (0,0) width 380: "The style declarations which apply to the text below are:"
      RenderBlock {PRE} at (0,38) size 768x68
        RenderText {#text} at (0,0) size 175x68
          text run at (0,0) width 175: "P {color: green ! important;}"
          text run at (175,0) width 0: " "
          text run at (0,17) width 84: "P {color: red;}"
          text run at (84,17) width 0: " "
          text run at (0,34) width 133: "P#id1 {color: purple;}"
          text run at (133,34) width 0: " "
          text run at (0,51) width 0: " "
      RenderBlock {HR} at (0,119) size 768x2 [border: (1px inset #000000)]
      RenderBlock {P} at (0,137) size 768x44 [color=#008000]
        RenderText {#text} at (0,0) size 729x44
          text run at (0,0) width 729: "This sentence should be green, because the declaration marked important should override any other normal"
          text run at (0,22) width 481: "declaration for the same element, even if it occurs later in the stylesheet."
      RenderBlock {P} at (0,197) size 768x66 [color=#008000]
        RenderText {#text} at (0,0) size 427x22
          text run at (0,0) width 427: "This sentence should also be green, even though it has an ID of "
        RenderInline {TT} at (0,0) size 20x17
          RenderText {#text} at (427,3) size 20x17
            text run at (427,3) width 20: "id1"
        RenderText {#text} at (447,0) size 755x66
          text run at (447,0) width 278: ", which would ordinarily make it purple. "
          text run at (725,0) width 30: "This"
          text run at (0,22) width 707: "is because declarations marked important have more weight than normal declarations given in a STYLE"
          text run at (0,44) width 63: "attribute."
      RenderBlock {P} at (0,279) size 768x44 [color=#008000]
        RenderText {#text} at (0,0) size 749x44
          text run at (0,0) width 648: "This sentence should also be green, even though it has a STYLE attribute declaring it to be red. "
          text run at (648,0) width 101: "This is because"
          text run at (0,22) width 703: "declarations marked important have more weight than normal declarations given in a STYLE attribute."
      RenderTable {TABLE} at (0,339) size 768x248 [border: (1px outset #808080)]
        RenderTableSection {TBODY} at (1,1) size 766x246
          RenderTableRow {TR} at (0,0) size 766x30
            RenderTableCell {TD} at (0,0) size 766x30 [bgcolor=#C0C0C0] [border: (1px inset #808080)] [r=0 c=0 rs=1 cs=2]
              RenderInline {STRONG} at (0,0) size 163x22
                RenderText {#text} at (4,4) size 163x22
                  text run at (4,4) width 163: "TABLE Testing Section"
          RenderTableRow {TR} at (0,30) size 766x216
            RenderTableCell {TD} at (0,123) size 12x30 [bgcolor=#C0C0C0] [border: (1px inset #808080)] [r=1 c=0 rs=1 cs=1]
              RenderText {#text} at (4,4) size 4x22
                text run at (4,4) width 4: " "
            RenderTableCell {TD} at (12,30) size 754x216 [border: (1px inset #808080)] [r=1 c=1 rs=1 cs=1]
              RenderBlock {P} at (4,4) size 746x44 [color=#008000]
                RenderText {#text} at (0,0) size 729x44
                  text run at (0,0) width 729: "This sentence should be green, because the declaration marked important should override any other normal"
                  text run at (0,22) width 481: "declaration for the same element, even if it occurs later in the stylesheet."
              RenderBlock {P} at (4,64) size 746x66 [color=#008000]
                RenderText {#text} at (0,0) size 427x22
                  text run at (0,0) width 427: "This sentence should also be green, even though it has an ID of "
                RenderInline {TT} at (0,0) size 20x17
                  RenderText {#text} at (427,3) size 20x17
                    text run at (427,3) width 20: "id1"
                RenderText {#text} at (447,0) size 741x66
                  text run at (447,0) width 274: ", which would ordinarily make it purple."
                  text run at (0,22) width 741: "This is because declarations marked important have more weight than normal declarations given in a STYLE"
                  text run at (0,44) width 63: "attribute."
              RenderBlock {P} at (4,146) size 746x66 [color=#008000]
                RenderText {#text} at (0,0) size 693x66
                  text run at (0,0) width 648: "This sentence should also be green, even though it has a STYLE attribute declaring it to be red. "
                  text run at (648,0) width 44: "This is"
                  text run at (0,22) width 693: "because declarations marked important have more weight than normal declarations given in a STYLE"
                  text run at (0,44) width 63: "attribute."
