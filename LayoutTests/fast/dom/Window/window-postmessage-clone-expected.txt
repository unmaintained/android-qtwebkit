Tests that we clone object hierarchies

PASS: 'postMessage(reallyDeepArray)' threw RangeError: Maximum call stack size exceeded.
PASS: 'postMessage(window)' threw TypeError: Type error
PASS: 'postMessage(({get a() { throw "x" }}))' threw x
PASS: eventData is null of type object
PASS: eventData is undefined of type undefined
PASS: eventData is 1 of type number
PASS: eventData is true of type boolean
PASS: eventData is 1 of type string
PASS: eventData is [object Object] of type object
PASS: eventData is [object Object] of type object
PASS: eventData is [object Object] of type object
PASS: eventData is [object Object] of type object
PASS: eventData is [object Object] of type object
PASS: eventData is [object Object] of type object
PASS: eventData is [object Object] of type object
PASS: eventData is [object Object] of type object
PASS: eventData is [object Object] of type object
PASS: eventData is of type object
PASS: eventData is a,a,b,a,b of type object
PASS: eventData is a,a,b,[object Object] of type object
PASS: eventData is 1,2,3 of type object
PASS: eventData is ,,1 of type object
PASS: eventData is null of type object
PASS: eventData is 2009-02-13T23:31:30.000Z of type object
PASS: eventData is [object Object] of type object
PASS: eventData is === to eventData.self
PASS: eventData is === to eventData[0]
PASS: eventData.graph1 is === to eventData.graph2
PASS: eventData[0] is === to eventData[1]
PASS: eventData is [object Array](default toString threw RangeError: Maximum call stack size exceeded.) of type object
PASS: eventData is [object File] of type object
PASS: eventData is [object FileList] of type object
PASS: eventData is [object ImageData] of type object
PASS: eventData is [object ImageData] of type object
PASS: eventData is done of type string

